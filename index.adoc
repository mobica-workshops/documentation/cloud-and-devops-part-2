:revealjs_theme: sky
:revealjs_plugin_pdf: enabled

:toc: left
:toclevels: 4:

:icons: font


= **Cloud & DevOps**

Part 2

== DevOps Engineer

* It is often difficult to understand this role because the DevOps Engineer is the product of a dynamic workforce that has not yet finished evolving. This is also main reason that career path required to become a DevOps Engineer is not clearly defined.
* To understand how we can help our clients with using DevOps methodology we need to better understand people who are called DevOps Engineers.

=== !

DevOps Engineer:

* must be an excellent communicator
* have role similar to IT Project Managers
* connects software development teams with IT operations and Security
* helps with bringing metrics to business

=== !

All of this is connected with tasks which are connected to this role:

* Scale cloud programs
* Create workflow processes
* Secure cloud programs and infrastructure
* Monitor cloud programs and infrastructure
* And more ...

[.notes]
--
* Monitoring is not only from the infrastructure perspective important here. In the end we should monitor also business
related data to help business with their KPIs
--

=== !

They work is mostly connected to the internal customers of the project like:

* QA and QC personnel
* Software and application developers
* Project managers
* Project stakeholders (usually from within project)

[.notes]
--
* Quality Assurance & Quality Control
--

=== !

A DevOps role in most organizations requires experience with:

* Integration technology
* Automation
* Cloud coding languages

Because this is to broad in many bigger organizations we start to have specialized DevOps Engineer Roles

[.notes]
--
* It is connected with huge amount of knowledge needed to achieve all tasks which are possible in this role. That is why we started to have specializations where people are starting focusing more on particular parts.
--

== DevOps Engineer Roles

* Cloud Infrastructure Engineer

[.step]
* Cloud Developer
* Site Reliability Engineer (SRE)
* DevOps Automation Engineer

[.notes]
--
* Cloud Infrastructure Engineer - this specialization is mostly connected to: Cloud Architects, Cloud DevOps, Cloud SysOps
* Cloud Developer - Developers who are using Cloud Infrastructure like for example Lambda Functions to create cloud programs
* SRE is similar to Infrastructure Engineer but it is role which is focusing more on Operations related tasks plus Automation. They are focusing on making service Reliable (stable) and are focusing on SLA
* DevOps Automation Engineer - They are focusing more on the software automation - pipelines
--

=== Cloud Infrastructure Engineer

* They focus on the components needed for cloud computing to be effective for their company and may work with software developers and hardware engineers as part of their duties
* This role is focusing on creation IaC (Infrastructure as Code) for a cloud software.

This role is very similar to SRE but they are in most of the cases creators not maintainers.

[.notes]
--
* In many cases especially in our company our Developers are having this role in addition to their default **Cloud Developer** role.
--

=== !

To be more precise they are focusing on:

* Adding users to a cloud infrastructure
* Adding permissions
* Adding or changing workflow processes
* Adding entire objects that function like containers (e.g., FaaS) to meet business needs

[.notes]
--
* FaaS - Function as a Service
--

=== Cloud Developer

* They focus on using Cloud Infrastructure in the most optimal way which is different depends on area they are working on. Any area like automotive, medicine, silicon have own and in many cases different needs
* They needs to have at least minimal knowledge about IaS code and tools - to create development infrastructure with a help of Cloud Infrastructure Engineer

[.notes]
--
* Cloud Developer is a good start to train Cloud Infrastructure Engineer if this person like to create code for the infrastructure. Other factor is that Cloud Infrastructure Engineer have currently high value in the market and it can be a promotion for a Developer
--

=== !

Most of Clouds give special services which are helping with creation of the software like:

  * Compute, Databases and Object Stores
  * Queues, Pub/Sub, Streaming solutions
  * Big Data
  * IoT and ML Services

[.notes]
--
* IoT - Internet of Things - this is big as it give many services used for many purposes
* ML - Machine Learning
--

=== Site Reliability Engineer (SRE)

A site reliability engineer (SRE):

* spend up to 50% of their time doing "ops"
* spend the other 50% of their time on development tasks

This role is mostly connected with maintaining and moving forward current infrastructure and services to be as reliable as possible.

[.notes]
--
* "ops" related work means: handling issues, spending time on-call, and manual intervention
* development tasks work means: adding new features - such as for scaling or automation
* in basics because software system that an SRE oversees is expected to be highly automatic and self-healing this person is moving infrastructure code to be fully automated and helping development teams to achieve self-healing not only of the infrastructure but also software which is working on it.
--

=== !

A site reliability engineer is usually either a software engineer with a good administration background or a skilled system administrator with knowledge of coding and automation. In most of the cases they are originated from administration background.

[.notes]
--
* This role is not our specialization we do not have currently to many people who can do this - solution can be starting involving our IT here and create future path for them as a promotion.
--

=== DevOps Automation Engineer

* They are focusing more on the Continuous Integration, Continuous Delivery and Continuous Deployment parts of the DevOps methodology and are
less fixed on the cloud and more on the tools which are used to achieve this automation.
* In our case they origin is mostly from DevQa as our DevQA is focusing on automation of CI and this is just next step for them.

[.notes]
--
* QA is like described upper best source for those specialists as they in many projects needs even to create from scratch own CI/CD infrastructure or at least upgrade and maintain it for a client. We just need to find people who know not only how to use CI but knows also how to crete it and are willing to learn automation tools
--

=== !

They job description in most of the situations looks like this:

* Design and Implement Continuous Integration Pipelines for Application Development projects
* Work with a variety of hosting approaches e.g. On Premises or Cloud based Platforms
* Enforce the proper execution of Agile paradigms in tandem with Management Tools
* Establish Automated Code Quality, Assembly Testing and Performance Testing Solutions

== Concepts and Practices

DevOps Automation Engineer is strongly focusing on:

* Build Automation
* Continuous Integration
* Continuous Delivery and Deployment

[.notes]
--
**Build Automation** - Development environment on laptops automated, CI and Dev laptops in sync as needs to use the same automation to achieve building environment

**Continuous Integration** - Self testing, Early detection possible, Frequent releases, Continuous testing, Test automation frameworks?

**Continuous Delivery and Deployment**: Delivery = code is deliverable (prepared for the deployment), Deployment = code is deployed automatically not only check if is ready for deployment
--

=== !

* Configuration management
* Orchestration

[.notes]
--
**Configuration management** - Minimize configuration drift, More maintainable infrastructure, More visibility about what is installed, Main part of the Build Automation

**Orchestration** - Automatization that support processes and workflows, such as provisioning resources, It brings scalability, stability, possibility of self-service, granular visibility of resources and it is saving a lot of time
--

=== !

Cloud Infrastructure Engineer is additionally or mainly focusing on:

* Infrastructure as a Code

[.notes]
--
**Infrastructure as a Code** - Manage infrastructure through code and automation, Because infrastructure is now treated as a code we can reuse this code in many environments like dev > pre > prod, We can version infrastructure
--

=== !

Site Reliability Engineer (SRE) is additionally or mainly focusing on:

* Monitoring
* Logging
* Tracing

[.notes]
--
* Collecting important data about infrastructure and services - Infrastructure Monitoring and Application Performance Monitoring (APM)
* Real time notifications based on the monitored data
* Post-mortem analysis based on the collected data
* Fast Recovery
* Better root cause analysis
* Visibility across teams
* Automated response for other tools like for example Orchestration
--

== Our Goal

* Upgrade our processes to be Cloud ready
* Train Developers / QA / IT with Cloud related technologies
* Learn how to react when client needs his project to be Cloud native

[.notes]
--
* At the current moment we have fragmented knowledge who is or was working in the projects which are connected to the cloud
* Next thing is that we do not know in many situations what we were doing in this project - what skill as a company we gained
* This in summary ends with problem in winning new types of projects which are cloud related
--

== Road for DevOps

There is no one road for the DevOps and our clients needs in many cases complete solution. Sometime is possible to start from one solution and in this cases there are 2 starting points:

* Starting with improving Development
* Starting with improving Infrastructure

=== Development

* Use infrastructure-as-code for self-service cloud workload deployments
* Connect CI/CD tools to automate and orchestrate siloed workflows
* Integrate with tools like Jenkins, Chef, Puppet and Ansible
* Develop, integrate and publish jobs as code directly from developer toolsets

=== !

* Make workloads a part of your DevOps lifecycle (dev, test, deploy)

[.step]
* In this situation most important people in the team are **Cloud Developers** and **DevOps Automation Engineers** and we
already have a lot experience in this kind of a projects and people with required knowledge.

[.notes]
--
* We need to focus on this road currently
--

=== Infrastructure

* Decrease service-ticket volumes
* Centralize control and visibility across complex workflows
* Empower developers with self-service
* Automate across on-prem, private and public cloud

=== !

* Proactively respond to failures with real-time alerts
* Drive actionable insights with powerful embedded analytics engine

[.step]
* In this situation we have only few projects we can show to our clients and very limited number of engineers as people required for this are **Cloud Infrastructure Engineers** and even sometimes **Site Reliability Engineers**.

[.notes]
--
* But we cannot forgot that this is also very important and should be a focus in the near future
--

== Sources

* https://www.bmc.com/blogs/devops-engineer-roles-and-responsibilities/# - without this post creating this presentation will be a nightmare
* https://www.stonebranch.com/it-automation-solutions/devops-automation

//[%notitle]
//[%conceal]
