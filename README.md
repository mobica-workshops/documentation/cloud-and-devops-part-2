# Cloud and DevOps Part 2

Development is based on the ASCII Doc format and `asciidoctor-reveal.js` module. Please read more about it here:

https://github.com/asciidoctor/asciidoctor-reveal.js/

Check `.gitlab-ci.yaml` to understand how to generate html version from adoc.

Starting dev server after this to see generated page:

    ruby -run -e httpd . -p 5000 -b 127.0.0.1
